---
title: "Alimentação Saudável"
date: 2021-10-23T22:01:17-03:00
draft: false
---

---
Salada mix de folhas, nozes e damasco
---
![](/layouts)
![](/images/salada500.jpg)

Ingredientes
---

- Folhas de meio maço de alface americana, rasgadas
- folhas de meio maço de rúcula, rasgadas
- folhas de meio maço de alface roxa, rasgadas
- folhas de meia unidade de raddichio, rasgadas
- 1 xícara (chá) de nozes picadas (100 g)
- meia xícara (chá) de damasco seco em tirinhas (70 g)
- 2 tomates médios cortados em gomos
- 1 xícara (chá) de queijo de cabra em bolinhas


Molho
---

- suco de 1 limão siciliano, coado
- meia xícara (chá) de Azeite de Oliva Extra Virgem
- 1 pitada de pimenta-do-reino moída (opcional)
- meia colher (chá) de AJI-SAL®

Modo de preparo
---

1. Arrume as folhas em uma saladeira funda. Junte as nozes, o damasco e o tomate, e misture delicadamente.

2. Prepare o molho: em uma tigela pequena, misture o suco de limão, 
Azeite de Oliva TERRANO™, o AJI-SAL® e a pimenta-do-reino, com o auxílio
de um garfo ou batedor de arame, até encorpar. Na hora de servir, regue o 
molho sobre a salada e acrescente o queijo de cabra. Misture 
delicadamente e sirva em seguida.

---
