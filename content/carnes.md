---
title: "Carnes"
date: 2021-10-23T22:01:17-03:00
draft: false
---

---
Filé ao Molho Madeira
---

![](/images/carne-molho-madeira500.jpg)

Ingredientes
---

- 1 pedaço pequeno de filé-mignon cortado em escalopes (400 g)
- Sal e pimenta-do-reino a gosto
- 2 colheres (sopa) de farinha de trigo
- 2 colheres (sopa) de manteiga
- 2 xícaras (chá) de caldo de carne forte (400 ml)
- 1/2 xícara (chá) de vinho Madeira seco (100 ml)
- 1 vidro de cogumelos fatiados (100 g)

Modo de preparo
---

1. Tempere os bifes com sal e pimenta e polvilhe metade da farinha, sem deixar excesso.
2. Frite-os na manteiga e reserve em local aquecido.
3. Na mesma frigideira, junte o restante da farinha dissolvida no caldo de carne, o vinho e os cogumelos.
4. Deixe ferver e encorpar, misturando sempre.
5. Espalhe o molho sobre os escalopes e sirva, se desejar, com arroz branco e purê de batata.

---
