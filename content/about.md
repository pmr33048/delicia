---
title: "Qual o nosso propósito?"
date: 2021-10-23T22:01:17-03:00
draft: false
---
Nós somos uma família de raízes italianas com o objetivo de trazer sabor a sua mesa, 

aqui você encontrará receitas deliciosas para compartilhar com quem mais importa.

As receitas foram divididas em quatro partes, bolos, carnes massas e uma incrível

linha de comidas saudáveis, agora basta você dar uma olhadinha e se delíciar.



---
